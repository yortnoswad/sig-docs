Containers built for ISA baseline and optimized are in the [ISA SIG containers registry](https://gitlab.com/CentOS/isa/containers/container_registry).

Use these containers how you would any other containers.

```
podman run -it registry.gitlab.com/centos/isa/containers/isabaseline
podman run -it registry.gitlab.com/centos/isa/containers/isaoptimized
```
