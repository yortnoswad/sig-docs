The ISA SIG uses the #centos-isa IRC channel on Libera.Chat for ad-hoc communication and work coordination.; this channel is also bridged on Matrix as [#centos-isa:fedoraproject.org](https://matrix.to/#/#centos-isa:fedoraproject.org). Official SIG meetings are also held on IRC.

See the [CentOS IRC documentation](https://wiki.centos.org/irc) for information on how to join IRC. For folks using Matrix we recommend following [this guide](https://kparal.wordpress.com/2021/06/01/connecting-to-libera-chat-through-matrix/).

For async discussions and announcements we generally use the [centos-devel](https://lists.centos.org/mailman/listinfo/centos-devel) mailing list.
